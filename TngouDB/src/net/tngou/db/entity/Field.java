package net.tngou.db.entity;


/**
 * 
* @ClassName: Field
* @Description: 字段类型
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年5月20日 下午2:44:17
*
 */
public class Field {

	private String name; //字段名称
	private String value;//字段值
	private Type type;//字段类型
	
	
	public Field(String name,String value,Type type) {
		this.name=name;
		this.value=value;
		this.type=type;
	}
	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getValue() {
		return value;
	}



	public void setValue(String value) {
		this.value = value;
	}



	public Type getType() {
		return type;
	}



	public void setType(Type type) {
		this.type = type;
	}


	@Override
	public String toString() {
		String s="name:"+name+"\n"
				+"value:"+value+"\n"
				+"type:"+type+"\n";
		
		return s;
	}
	
	/**
	 * 
	* @ClassName: Type
	* @Description: 字段类型 String 不分词  ，Text 分词 ， Key 主键 
	* @author tngou.ceo@aliyun.com
	* @date 2014年12月11日 上午11:07:54
	*
	 */

  public enum Type{
	   //String 不分词  ，Text 分词 ， Key 主键 
		Key,String,Text
	}
}
