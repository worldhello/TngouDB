package net.tngou.db.netty;

import java.io.File;

import net.tngou.db.i18n.I18N;
import net.tngou.db.lucene.LuceneManage;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 *  关闭服务器
* @ClassName: StopServer
* @Description: TODO(这里用一句话描述这个类的作用)
* @author tngou@tngou.net
* @date 2015年5月14日 上午9:48:06
*
 */
public class StopServer extends Netty{
	private static Logger log = LoggerFactory.getLogger(StopServer.class);
	@Override
	public void run() {
	
		try {
			 Parameters params = new Parameters();
				FileBasedConfigurationBuilder<?> builder =
				    new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
				    .configure(params.properties()
				        .setFileName("config.properties"));
			Configuration config = builder.getConfiguration();

			config.setProperty("run", 0);
			builder.save();
		} catch (ConfigurationException e) {
			log.error(I18N.getValue("stop_error","config.properties"));
			e.printStackTrace();
		}finally
		{
			Thread.yield();
		}
		
	}
	
	
	//关闭线程
	public static void main(String[] args) {
		
		new StopServer().run();
		log.info(I18N.getValue("stop"));
	}
}
