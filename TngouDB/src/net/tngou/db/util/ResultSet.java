package net.tngou.db.util;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class ResultSet implements Serializable{

	/**
	 * 
	 * TngouDB 返回相关的编码
	 *  <br>
	 *  1**：请求收到，继续处理    process 
	 * 	2**：操作成功收到，分析、接受 success
	 * 	3**：完成此请求必须进一步处理 complete
	 * 	4**：请求包含一个错误语法或不能完成 error
	 * 	5**：服务器执行一个完全有效请求失败  failure
	 *  <br>
	 *  
	 * 
	 * 

————–

100——客户必须继续发出请求，原来的请求为  "" 空
101——客户要求服务器根据请求协议版本不一样

————–

200——请求成功，成功返回相关数据
201——请求成功，授权认证
202——接受和处理、但处理未完成
203——返回信息不确定或不完整
204——请求收到，但返回信息为空
205——服务器完成了请求，再次请求完整
206——服务器已经完成了部分用户的请求

————–

300——请求的资源可在多处得到
301——删除请求数据
302——在其他地址发现了请求数据
303——建议客户访问其他协议或访问方式
304——客户端已经执行了请求，但文件未变化
305——请求的资源必须从服务器指定的地址得到
306——前一版本中使用的代码，现行版本中不再使用
307——申明请求的资源临时性删除
————–
400——错误请求，如语法错误
401——请求授权失败
402——错误请求
403——请求不允许
404——链接服务器错误
405——SQL字段定义的方法不允许
406——请求资源不可访问,访问的表不存在
407——类似401，用户必须首先在代理服务器上得到授权
408——客户端没有在用户指定的饿时间内完成请求
409——对当前资源状态，请求不能完成
410——服务器上不再有此资源且无进一步的参考地址
411——服务器拒绝用户定义的Content-Length属性请求
412——一个或多个请求头字段在当前请求中错误
413——请求的资源大于服务器允许的大小
414——请求的资源URL长于服务器允许的长度
415——请求资源不支持请求项目格式
416——请求中包含Range请求头字段，在当前请求资源范围内没有range指示值，请求也不包含If-Range请求头字段
417——服务器不满足请求Expect头字段指定的期望值，如果是代理服务器，可能是下一级服务器不能满足请求
—————
500——服务器产生内部错误
501——服务器不支持请求的函数
502——服务器暂时不可用，有时是为了防止发生系统过载
503——服务器过载或暂停维修
504——关口过载，服务器使用另一个关口或服务来响应用户，等待时间设定值较长
505——服务器不支持或拒绝支请求头中指定的版本


	 */
	private static final long serialVersionUID = 1L;
	
	public static int T_PROCESS_CONTIUE=100;
	public static int T_PROCESS_VERSION=101;
	
	public static int T_SUCCESS_OK=200;  
	public static int T_SUCCESS_NOT_AUTH=201;
	public static int T_SUCCESS_NOT_FINISHED=202;
	public static int T_SUCCESS_NOT_FULL=203;
	public static int T_SUCCESS_NULL=204;
	public static int T_SUCCESS_AGAIN=205;
	public static int T_SUCCESS_PART=206;
	
	public static int T_COMPLETE_WAIT=300;
	public static int T_COMPLETE_DELETE=301;
	public static int T_COMPLETE_OTHER=302;
	public static int T_COMPLETE_OTHER_VERSION=303;	
	public static int T_COMPLETE_NOT_FILE=304;
	public static int T_COMPLETE_NOT_REQUEST=305;
	public static int T_COMPLETE_LAST_VERSION=306;
	public static int T_COMPLETE_FILE_DELETE=307;
	
	public static int T_ERROR_SQL=400;
	public static int T_ERROR_AUTH=401;
	public static int T_ERROR_REQUEST=402;
	public static int T_ERROR_ALLOW=403;
	public static int T_ERROR_CONNECT=404;
	public static int T_ERROR_SQL_ALLOW=405;
	public static int T_ERROR_READ=406;
	public static int T_ERROR_NOT_AUTH=407;
	public static int T_ERROR_FINISHED=408;
	public static int T_ERROR_NOT_FINISHED=409;
	
	
	public static int T_FAILURE_SYSTEM=500;
	public static int T_FAILURE_SQL=501;
	public static int T_FAILURE_REFUSE=502;
	public static int T_FAILURE_STOP=503;
	public static int T_FAILURE_WAIT=504;
	public static int T_FAILURE_VERSION=505;
	
	private int status=T_SUCCESS_OK;  //返回状态
	private String msg ; //提示，，一般是指错误提示返回
	
	private int size;
	private int total;
	private int start=0;
	private int page;
	private List<Map<String, Object>> list;
	
	public ResultSet() {
		
	}
	public ResultSet(int status) {
		this.status =status;
	}
	public ResultSet(int status,String msg) {
		this.status =status;
	}
	
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
	
	
	@Override
	public String toString() {
		String s="----------------------------------------\n"
				+ "status:"+status+"    "
				+ "start:"+start+"   "
				+ "size:"+size+"   "
				+ "page:"+page+"   "
				+ "total:"+total+"  \n"
				+ "msg:"+msg+"\n";
		if(list!=null)
		{
			for (Map<String, Object> map : list) {
				String maps="**************************************\n";
				Set<Entry<String, Object>> sets = map.entrySet();
				for (Entry<String, Object> entry : sets) {
					maps+=entry.getKey()+":"+entry.getValue()+"          \n";
					
				}	
				s+=maps;
			}
			
		}
	     s+="----------------------------------------\n";
		return s;
	}
	
	
	
}
