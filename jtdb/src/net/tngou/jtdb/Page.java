package net.tngou.jtdb;


import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import net.tngou.db.util.ResultSet;


/**
 * 分页
* @ClassName: Page
* @Description: TODO(这里用一句话描述这个类的作用)
* @author tngou.ceo@aliyun.com
* @date 2015年5月5日 下午4:00:44
*
 */
public class Page 
{
	 private List<Map<String, Object>> list;
	 private int page;
	 private int start;
	 private int size;
	 private int totalpage;
	 private int total;
	 private int status=ResultSet.T_SUCCESS_OK;  //返回状态
	 public Page() {
		// TODO Auto-generated constructor stub
	}
	 
	 public Page(int start ,int size ,int total,List<Map<String, Object>> list) 
	 {
			this.page =start%size==0?start/size:(start/size+1);
			this.size = size;
			this.total= total;
			this.totalpage = total%size==0?total/size:(total/size+1);
			this.start=start;
			
			this.list = list;
		}
	 
	 
	 
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getTotalpage() {
		return totalpage;
	}
	public void setTotalpage(int totalpage) {
		this.totalpage = totalpage;
	}



	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	@Override
	public String toString() {
		String s="----------------------------------------\n"	
				+ "start:"+start+"   "
				+ "size:"+size+"   "
				+ "page:"+page+"   "
				+ "total:"+total+"  \n";
		if(list!=null)
		{
			for (Map<String, Object> map : list) {
				String maps="**************************************\n";
				Set<Entry<String, Object>> sets = map.entrySet();
				for (Entry<String, Object> entry : sets) {
					maps+=entry.getKey()+":"+entry.getValue()+"          \n";		
				}	
				s+=maps;
			}
			
		}
	     s+="----------------------------------------\n";
		return s;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	 

}
